//
//  H4X0R_NewsApp.swift
//  H4X0R News
//
//  Created by  Amber  on 10/20/21.
//

import SwiftUI

@main
struct H4X0R_NewsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
